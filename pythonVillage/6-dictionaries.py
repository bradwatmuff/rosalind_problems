import sys

#open text file
f = open("input.txt", 'r')
#read lines from file and split each word into a list
words = f.readline()
wordList = words.split()
#iterate over the list and add new words to dictionary, increase the value by one if it is already in the dictionary
dictionary = {}
for i in wordList:
    if i in dictionary:
        dictionary[i] += 1
    else:
        dictionary.update({i: 1})
#output the sample and write to a new file
sys.stdout =  open("output.txt", "w")
for j in dictionary:
             print(j, dictionary[j])
sys.stdout.close()