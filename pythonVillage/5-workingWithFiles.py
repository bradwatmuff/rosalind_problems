#Open file
file = open('input.txt', 'r')
#read lines from file and add each to a list
lines = file.readlines()
print(lines)
#iterate over the list and keep even numbered lines
evenLines = lines[1::2]
print(evenLines)
#write a file where each item in the even list becomes a new line
fileOut = open('output.txt', 'w')
for i in evenLines:
    fileOut.write(str(i))
