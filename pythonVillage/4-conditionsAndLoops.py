# input data
firstNum = int(input("Enter first number of data set:\n"))
secondNum = int(input("Enter second number of data set:\n"))
# create list with just odd numbers
numList = [i for i in range(firstNum, secondNum)]
# loop through list and add to a variable
oddList = []
for num in numList:
    if num % 2 != 0:
        oddList.append(num)
totalOdd = 0
for odd in oddList:
    totalOdd += odd
# output final result
print("The total of all odd numbers in the sequence from", firstNum, "to", secondNum, "is:")
print(totalOdd)