#input string and substring
with open("input.txt") as myFile:
    data = myFile.readlines()
print(data)
s = data[0].strip()
ss = data[1].strip()
#loop over all letters in string and test whether a letter is the beginning of substring
for i in range(len(s)):
    if s[i:].startswith(ss):
        print(i+1, end=" ")
