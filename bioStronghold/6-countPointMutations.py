seq = [line.strip('\n') for line in open('test_data/hamming.txt')]
hamming = 0                                                
for nt1, nt2 in zip(seq[0],seq[1]):                        
    if nt1 != nt2:                                         
        hamming += 1                                       
print(hamming)