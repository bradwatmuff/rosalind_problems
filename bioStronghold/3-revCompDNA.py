DNA = input("please enter your DNA sequence \n")
DNA_upper = DNA.upper()
DNA_up_rev = DNA_upper[::-1]
print("your reverse complement is \n")
for i in DNA_up_rev:
    if (i == "A"): print("T", end = "")
    elif (i == "T"): print("A", end = "")
    elif (i == "G"): print("C", end = "")
    elif (i == "C"): print("G", end = "")
