#Fibonnaci loop

def Fibonacci_Loop_Pythonic(months, offspring):
    parent, child = 1, 1
    for i in range(months - 1):
        child, parent = parent, parent + (child * offspring)
    return child

#Modify Fibonnaci loop to reflect rabbit problem

n = 4 #replace input                                                                        
m = 3 #replace input                                                                       
bunnies = [1, 1]                                                               
months = 2
count = []                                                                     
while months < n:                                                              
    if months < m:                                                             
        bunnies.append(bunnies[-2] + bunnies[-1])                              
    elif months == m or count == m + 1:                                        
        bunnies.append(bunnies[-2] + bunnies[-1] - 1)                          
    else:                                                                      
        bunnies.append(bunnies[-2] + bunnies[-1] - bunnies[-(                  
            m + 1)])                                                           
    months += 1                                                               
print(bunnies[-1])

#enter variables into function
#return rabbit and time values in formatted way
