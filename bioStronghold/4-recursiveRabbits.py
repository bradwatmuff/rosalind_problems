def fibLoopRab(months, offspring):
    parent, child = 1, 1
    for _ in range(months - 1):
        child, parent = parent, parent + (child * offspring)
    return child

print(fibLoopRab(900, 6))
